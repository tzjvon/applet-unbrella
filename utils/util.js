function formatTime(date,symbol) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join(symbol) + '  ' + [hour, minute, second].map(formatNumber).join(':');

}
function nowDate() {
  var date = new Date();
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-');
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}
function formatYM(_d) {
  var date = new Date(_d);
  var year = date.getFullYear()
  var month = date.getMonth() + 1

  return year + "年" + month + "月";
}
function formatYMD(_d) {
  var date = new Date(_d);
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  return year + "年" + month + "月" + day + "日";
}
function formatHMS(_d) {  /* 传入时间戳 */
  var dt = new Date(_d);
  var hour = dt.getHours();
  var minite = dt.getMinutes();
  var second = dt.getSeconds();
  return hour + ':' + minite + ':' + second
}
function formatMinite(_d) {  /* 传入时间戳 */
  var hourStamp = 1000 * 60 * 60,
      miniteStamp = 1000 * 60,
      secondStamp = 1000;

  var hour = Math.floor(_d / hourStamp),
      minite = Math.floor((_d - hour * hourStamp) / miniteStamp);

  return minite
}

function POST(_url, _data, fn) {
  wx.request({
    url: getApp().globalData.mainDomain + _url,
    data: _data,
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    method: 'POST',
    success: fn,
    fail: function (res) { },
    complete: function (res) { }
  })
}
function toast(txt) {
  wx.showToast({
    title: txt,
    image: '../../images/icons/tips.png'
  })
}

function call(number) {
  wx.makePhoneCall({
    phoneNumber: number
  })
}

module.exports = {
  formatTime: formatTime,

  formatHMS: formatHMS,
  formatMinite: formatMinite,

  formatYM: formatYM,
  formatYMD: formatYMD,
  nowDate: nowDate,
  POST: POST,

  toast: toast,

  servicePhone: '4001026669',
  call: call,

  session: wx.getStorageSync(getApp().globalData.storageKey)

}
