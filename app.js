App({
    onLaunch: function (e) {

    },
    getUserInfo: function(cb){
        var that = this
        // 检测是否为登录状态
        wx.checkSession({
            success: function(res) {
                // 获取缓存用户信息
                wx.getStorage({
                    key: that.globalData.storageKey_UserInfo,
                    success: function (res) {
                        that.globalData.userInfo = res.data
                        typeof cb == "function" && cb(that.globalData.userInfo)
                    },
                    fail: function (res) {
                        that.login(cb)
                    }
                })
            },
            fail: function(res) {
                that.login(cb)
            },
            complete: function(res) {},
        })

    },
    login : function(cb){
        var that = this
        wx.login({
            success: function (rres) {
                // console.log(rres);
                // 存储openid session_key
                that.getTrdSession(rres.code);

                typeof cb == "function" && cb(that.globalData.userInfo)
            },
            fail : function(rres){
                that.login(cb)
            }
        })
    },
    setStorage:function(trdKey,trdVal){
        var that = this
        wx.setStorage({
            key: trdKey,
            data: trdVal,
            success: function (res) { },
            fail: function (res) { },
            complete: function (res) { },
        })
    },
    // 获取第三方sessionId
    getTrdSession : function(code){

        console.log(code);

        var that = this
        wx.request({
            url: that.globalData.mainDomain + 'wxapp/getWxSessionByCode',
            data: {'code':code},
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: 'POST',
            success: function (res) {
                that.globalData.userSession = res.data
                that.setStorage(that.globalData.storageKey,res.data);
                that.globalData.userSession = res.data;
                // 获取用户信息
                wx.getUserInfo({
                    success: function (res) {
                        that.globalData.userInfo = res.userInfo
                        that.setStorage(that.globalData.storageKey_UserInfo, res.userInfo);
                        // 存储用户信息
                        that.saveUserInfo(res.iv,res.rawData,res.encryptedData,res.signature)
                    }
                })
            },
            fail: function (res) { },
            complete: function (res) { },
        })
    },
    // 用户信息存储
    saveUserInfo: function (iv, rawDagta, encryptedData,signature){
        var that = this
        var userSession = that.globalData.userSession
        wx.request({
            url: that.globalData.mainDomain + 'wxapp/saveWxAppUserInfo?userSession=' + userSession,
            data: {
                'iv': iv,
                'rawDagta': rawDagta,
                'encryptedData': encryptedData,
                'signature': signature},
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: 'POST',
            success: function (res) {

            },
            fail: function (res) { },
            complete: function (res) { }
        })
    },
    globalData:{
        storageKey_UserInfo:'nyb_trd_userInfo',
        storageKey:'nyb_trd_session',
        userSession:null,
        userInfo:null,
        // mainDomain:'https://weixin.aliautoservice.com/',
        // mainDomain:'https://weixin.aliautoservice.com/',
        mainDomain:'http://192.168.0.101:8090/',
        // mainDomain:'192.168.0.102:8090/',
    }
})