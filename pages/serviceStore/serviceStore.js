Page({
    data: {
        selectDisplay: false,
        pullUp: false
    },

    switchSelectDisplay: function () {
        this.setData({
            selectDisplay: !this.data.selectDisplay
        })
    },

    hideSelectDisplay: function () {
        this.setData({
            selectDisplay: false
        })
    },

    onLoad: function (opts) {


    },

    onReachBottom: function (opts) {
        var self = this

        if (self.data.pullUp) {
            return
        }

        self.setData({
            pullUp: true
        })

        setTimeout(function () {
            console.log(10)
            self.setData({
                pullUp: false
            })
        }, 1000)
    }

})