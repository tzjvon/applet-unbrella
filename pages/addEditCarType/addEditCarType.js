var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    setDefault: false,

    focus: false,
    selected: false,
    radioUrl: "../../images/addEditCarType/select.png",
    radioActiveUrl: "../../images/addEditCarType/selected.png",

    regTimeStr: null,
    mileage: null,
    endDate: util.nowDate(),
    modeId: null,
    carTypeFullName: null,
    userSession: null,


    carTypePrice: null,
    focus_pri: false,
  },

  switchDefault(e) {
    this.setData({
      setDefault: !this.data.setDefault
    })
  },

  switchRadio: function (e) {
    if (this.data.selected) {
      this.setData({
        selected: false
      })
    }else {
      this.setData({
        selected: true
      })
    }
  },

  focus: function (e) {
    this.setData({
      focus: true
    })
  },
  input: function (e) {
    this.setData({
      mileage: e.detail.value
    })
  },
  focus_price: function (e) {
    this.setData({
      focus_pri: true,
    })
  },
  input_price: function (e) {
    this.setData({
      carTypePrice: e.detail.value
    })
  },

  bindDateChange: function (e) {
    this.setData({
      regTimeStr: e.detail.value
    });
  },

  btnSave: false,

  saveYBPlan: function (e) {
    var that = this;

    if (!that.data.modeId) {
      util.toast('请选择您的车型');
      return false;
    }
    if (!that.data.regTimeStr) {
      util.toast('请选择初次登记时间');
      return false;
    }
    if (!that.data.mileage) {
      util.toast('请输入您的里程数');
      return false;
    }
    if (!that.data.carTypePrice) {
      util.toast('请输入购买金额');
      return false
    }

    var obj = {
      userSession: that.data.userSession,
      modelId: that.data.modeId,
      // regTimeStr: that.data.regTimeStr.substr(0, that.data.regTimeStr.length-3),
      regTimeStr: that.data.regTimeStr,
      mileage: that.data.mileage,
      isOperate: !that.data.selected ? 1 : 0,
      productPrice: that.data.carTypePrice
    };

    util.POST('wxapp/userAutos/saveUserAutos', obj, function (e) {
      if (that.btnSave) {return;}
      that.btnSave = true;

      wx.navigateTo({
        url: '/pages/settlement/settlement?id='+e.data.data.userAutos.id + '&modelId=' + e.data.data.userAutos.modelId + '&isNew=' + e.data.data.userAutos.isNew
      });
      that.btnSave = false;

    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },



  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})