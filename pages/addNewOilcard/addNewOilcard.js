Page({
    data: {
    	selectBrandIndex: 0
    },

    onLoad: function (opts) {


    },

    tapBrand: function (e) {
    	var index = e.currentTarget.dataset.index
    	if (index != this.data.selectBrandIndex) {
    		this.setData({
    			selectBrandIndex: index
    		})
    	}
    }

})