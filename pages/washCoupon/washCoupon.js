const util = require('../../utils/util')

Page({
    data: {
        isLoading: true,

        couponList: [],

        qrDisplay: false


    },

    page: 0,
    stopLoadCoupon: false,

    loadCoupon: function (self) {
        if (self.stopLoadCoupon) { return }
        util.POST('userCoupon/findUserCouponPg',{
            userSession: util.session,
            page: self.page++,
            rows: 4,
            couponType: 2
        }, function (e) {
            if (self.data.isLoading) {
                self.setData({
                    isLoading: false
                })
            }

            var userCoupons = e.data.data.userCoupons;
            console.log( userCoupons );
            if (userCoupons.length <= 0) {
                self.stopLoadCoupon = true
                return
            }
        })
    },

    toggleQR: function () {
        this.setData({
            qrDisplay: !this.data.qrDisplay
        })
    },

    onLoad: function (opts) {
        // this.loadCoupon(this)
        this.setData({
            isLoading: false
        })
    },

    onReachBottom: function (e) {

    }

})