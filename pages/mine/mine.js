var util = require('../../utils/util.js')
var app = getApp();

Page({
    data: {
        isLoading: true,
        showPop: false,

        isLimit: false,
        timeCountDown: 60,

        phoneNumber: null,
        phoneCorrect: false
    },

    interval: null,

    onLoad: function (opts) {
        this.setData({
            isLoading: false
        })
    },

    showPop: function () {
        this.setData({
            showPop: !this.data.showPop
        })
    },

    hidePop: function () {
        this.setData({
            showPop: false
        })
    },

    countDown: function () {
        if (this.data.isLimit || !this.data.phoneCorrect) {return}

        console.log('send code')

        util.toast("验证码在20分钟内有效")

        this.setData({
            isLimit: true
        })

        this.interval = setInterval(function () {
            if (this.data.timeCountDown <= 0) {
                this.setData({
                    isLimit: false,
                    timeCountDown: 60
                })
                return clearInterval(this.interval)
            }
            this.setData({
                timeCountDown: --this.data.timeCountDown
            })
        }.bind(this), 1000)
    },


    phoneNumberBlur: function (e) {
        console.log('input blur')

        var value = e.detail.value

        if (value.toString().match(/^1(3|4|5|6|7|8){1}[0-9]{9}/)) {
            this.setData({
                phoneCorrect: true,
                phoneNumber: value
            })
        }else{
            this.setData({
                phoneCorrect: false,
                phoneNumber: value
            })
            util.toast('手机号格式不正确')
        }

    }

})