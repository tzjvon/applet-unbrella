Page({
    data: {
    	brandSelected: 0
    },

    onLoad: function (opts) {


    },

    tapBrand: function (e) {
    	var index = e.currentTarget.dataset.index
    	if (this.data.brandSelected != index) {
    		this.setData({
    			brandSelected: index
    		})
    	}
    }

})