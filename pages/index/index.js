var app = getApp();

Page({
    data: {
        isLoading: true
    },

    video: null,
    isplay: false,

    videoClick: function () {
        if (!this.video) {return}
        if (this.isplay) {
            this.video.play()
        }else {
            this.video.pause()
        }

        this.isplay = !this.isplay

    },

    onLoad: function (opts) {
        this.setData({
            isLoading: false
        })
    },

    onReady: function () {
        // this.video = wx.createVideoContext('v1')
    }

})