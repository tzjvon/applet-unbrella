var util = require('../../utils/util');

Page({

  data: {

    userName: null,
    userPhone: null,
    addressdetail: null,


    addressInfo: null,

    pickerValue: [0,0,0],

    valAddress: '',
    addOn: false,
    province: null,
    city: null,
    town: null,
    pickerADD: null,

    uname: null,
    uphone: null,
    addressDetail: null,

    preArr: null
  },

  urlProvince: "addressDict/selectAddressDictByModel",
  urlCities: "addressDict/selectAddressDictByModel",
  urlTown: "addressDict/selectAddressDictByModel",

  user: {
      name: false,
      phone: false,
      address: false,
      addDetail: false
  },

  levelType: function (level) {
      var lv = [
          urlProvince,
          urlCities,
          urlTown
      ]
      return lv[level]
  },

  showSelector: function (e) {
    this.setData({
      addOn: true
    });

    this.user.address = true;

    var arr = this.data.pickerValue,
        province = this.data.province,
        city = this.data.city,
        town = this.data.town;

    this.setData({
        valAddress: province[arr[0]].name + ' ' + city[arr[1]].name + ' ' + town[arr[2]].name
    })
    this.updataAddress(this.data.pickerValue);
  },

  hideSelector: function (e) {
    this.setData({
        addOn: false
    })
  },

  btnSure: function (e) {
        var fakeAdd = this.data.fakeAddress;

        this.hideSelector(e);
        this.setData({
            valAddress: fakeAdd
        })
    },

  username: function (e) {
      var val = e.detail.value;

      if (val.length > 0) {
          this.user.name = true;
          this.setData({
              uname: val
          })
      }else {
          this.user.name = false;
      }
  },

  Phone: function (e) {
      var val = e.detail.value;
      var isok = val.match(/^[1][34578]{1}[0-9]{9}$/);
      if (!isok) {
          util.toast('手机号格式不正确')
      }else {
          this.user.phone = true
      }

      this.setData({
          uphone: val
      })
  },

  addDetail: function (e) {
      var val = e.detail.value;

      if (val.length > 0) {
          this.user.addDetail = true
      }else {
          this.user.addDetail = false;
      }

      this.setData({
          addressDetail: val
      })
  },

  btnSave: function (e) {
      var that = this;
      var pass = false;

      var arr = Object.keys(this.user),
          len = arr.length;
      for (var i = 0; i < len; i++) {

          if (!that.user[arr[i]]) {
              switch (i) {
                  case 0:
                      util.toast('请完善您的姓名')
                      break;
                  case 1:
                      util.toast('请完善您的手机号码')
                      break;
                  case 2:
                      util.toast('请完善选择地址')
                      break;
                  case 3:
                      util.toast('请完善详细地址')
                      break;
              }
              return;
          }
      }

      var session = wx.getStorageSync(getApp().globalData.storageKey);

      var param = {
          userSession: session,
          provinceId: that.data.pickerADD.province.id,
          provinceName: that.data.pickerADD.province.name,
          cityId: that.data.pickerADD.city.id,
          cityName: that.data.pickerADD.city.name,
          countyId: that.data.pickerADD.town.id,
          countyName: that.data.pickerADD.town.name,
          addressDetail: that.data.addressDetail,
          receiveName: that.data.uname,
          receivePhone: that.data.uphone
      };

      util.POST('wxapp/userAddress/saveUserAddress',param , function (e) {

          if (e.data.success) {
              wx.redirectTo({
                url: '/pages/myAddress/myAddress'
              });
          }
      })

  },

  compareTwoArr: function (arr1, arr2) {
      var len1 = arr1.length,
          len2 = arr2.length;
      if (len1 !=len2) { return null;  }
      for (var i = 0;i < len1; i++) {
          if ( arr1[i] !== arr2[i] ) {
              return i;
          }
      }
  },

  bindChange: function (e) {
      var that = this;

      var val = e.detail.value,
          level = 0;

      if (this.preArr) {
          level = this.compareTwoArr(val, this.preArr)
      }

      var index = e.detail.value[level]

      this.preArr = val;

      if (level == 0) {         // province
          this.initAddress( 1, this.data.province[index].id, function () {
              that.setData({
                  pickerValue: [val[0], 0, 0],
              });
              that.updataAddress(val)
          } )
      }else if (level == 1) {   // city
          this.initAddress( 2, this.data.city[index].id, function () {
              that.setData({
                  pickerValue: [val[0], val[1], 0]
              });
              that.updataAddress(val)
          } )
      }else if (level == 2) {   // town
          this.updataAddress(val);
      }



  },

  updataAddress: function(val) {
      var that = this;

      that.setData({
            pickerADD: {
                province: {
                    name: that.data.province[val[0]].name,
                    id: that.data.province[val[0]].id
                },
                city: {
                    name: that.data.city[val[1]].name,
                    id: that.data.city[val[1]].id
                },
                town: {
                    name: that.data.town[val[2]].name,
                    id: that.data.town[val[2]].id
                }
            },
            valAddress: that.data.province[val[0]].name + ' ' +
            that.data.city[val[1]].name + ' ' +
            that.data.town[val[2]].name
        });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initAddress(0);
  },

  initAddress: function (level, _parentId, callback) {
      var that = this;

      var addProvince = [];

      switch (++level) {
          case 1:
              util.POST(that.urlProvince, {levelType: level}, function (e) {

                  // console.log(e.data.data.addressDicts)

                  var provinceData = e.data.data.addressDicts;
                  provinceData.forEach(function (item) {
                      addProvince.push({
                          name: item.name,
                          id: item.id
                      })
                  })

                  that.setData({
                      province: addProvince
                  })


                  // console.log(provinceData[0].id);

                  util.POST(that.urlCities, {"parentId": provinceData[0].id}, function (e) {
                      var citiesData = e.data.data.addressDicts;
                      var addCities = [];

                      // console.log(citiesData)

                      citiesData.forEach(function (item) {
                          addCities.push({
                              name: item.name,
                              id: item.id
                          })
                      })

                      that.setData({
                          city: addCities
                      })


                      util.POST(that.urlTown, {"parentId": citiesData[0].id}, function (e) {
                          var addTown = [];
                          var townData = e.data.data.addressDicts;

                          // console.log(townData)

                          townData.forEach(function (item) {
                              addTown.push({
                                  name: item.name,
                                  id: item.id
                              })
                          })

                          that.setData({
                              town: addTown
                          })

                          if (typeof callback == 'function') {
                              callback();
                          }

                      })



                  })
              })
              break;
          case 2:
              util.POST(that.urlCities, {"parentId": _parentId}, function (e) {
                      var citiesData = e.data.data.addressDicts;
                      var addCities = [];

                      // console.log(citiesData)

                      citiesData.forEach(function (item) {
                          addCities.push({
                              name: item.name,
                              id: item.id
                          })
                      })

                      that.setData({
                          city: addCities
                      })

                      util.POST(that.urlTown, {"parentId": citiesData[0].id}, function (e) {
                          var addTown = [];
                          var townData = e.data.data.addressDicts;

                          // console.log(townData)

                          townData.forEach(function (item) {
                              addTown.push({
                                  name: item.name,
                                  id: item.id
                              })
                          })

                          that.setData({
                              town: addTown
                          })
                          if (typeof callback == 'function') {
                              callback();
                          }

                      })
                  })
              break;
          case 3:
              util.POST(that.urlTown, {"parentId": _parentId}, function (e) {
                          var addTown = [];
                          var townData = e.data.data.addressDicts;

                          townData.forEach(function (item) {
                              addTown.push({
                                  name: item.name,
                                  id: item.id
                              })
                          })

                          that.setData({
                              town: addTown
                          })

                          if (typeof callback == 'function') {
                      callback();
                  }


              })
              break;
      }

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})